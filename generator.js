const symbols =
  "abcdefghijklmnopqrstquvwxyzABCDEFGHIJKLMNOPQRSTQVWXYZ0123456789+-/=&#^?.,!@'{}[]";

function generate(length, forbidden) {
  if (length <= 0 || !length) length = 16;
  let chars = excludeForbidden(symbols, forbidden);
  var pass = new Array(length);
  for (var i = 0; i < length; i++) {
    let num = parseInt(Math.random() * chars.length);
    if (num >= chars.length) num = chars.length - 1;
    pass[i] = chars[num];
  }
  return pass.join("");
}

function excludeForbidden(str, forbidden) {
  if (!forbidden) return str;
  var result = str.split("");
  for (var i = 0; i < forbidden.length; i++) {
    for (var j = 0; j < result.length; j++) {
      if (result[j] === forbidden[i]) {
        result.splice(j, 1);
      }
    }
  }
  return result.join("");
}

// For testing
module.exports = {
  symbols,
  generate,
  excludeForbidden
};
