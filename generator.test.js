const utils = require("./generator");

describe("generate(8)", () => {
  it("Should generate 8-symbol long password", () => {
    expect(utils.generate(8).length).toBe(8);
  });
});

describe('generate(10, {all symbols but "a"})', () => {
  let forbidden = utils.symbols.slice(1, utils.symbols.length);

  it('Should generate 10-symbol long string that contains only letter "a"', () => {
    let password = utils.generate(10, forbidden);
    expect(password.length).toBe(10);
    expect(password).toEqual(new Array(10).fill("a").join(""));
  });
});

describe('generate(undefined, {all symbols but "a"})', () => {
  let forbidden = utils.symbols.slice(1, utils.symbols.length);

  it('Should generate 16-symbol long string that contains only letter "a"', () => {
    let password = utils.generate(undefined, forbidden);
    expect(password.length).toBe(16);
    expect(password).toEqual(new Array(16).fill("a").join(""));
  });
});

describe("generate()", () => {
  it("Should generate 16-symbol long password", () => {
    let password = utils.generate();
    expect(password.length).toBe(16);
  });
});

describe('excludeForbidden("abcd", "a")', () => {
  it('Should remove symbol "a" from string "abcd"', () => {
    expect(utils.excludeForbidden("abcd", "a")).toEqual("bcd");
  });
});

describe('excludeForbidden("abcd")', () => {
  it('Should remove no symbols from string "abcd"', () => {
    expect(utils.excludeForbidden("abcd")).toEqual("abcd");
  });
});
