var total = 0;

document.getElementById("generate").addEventListener("click", () => {
  var amount = parseInt(document.getElementById("amount").value);
  var forbidden = document.getElementById("forbidden").value;

  console.log(amount, forbidden);
  var password = document.createElement("div");
  password.className = "password";
  password.innerText = generate(amount, forbidden);
  total++;
  if (total >= 10) {
    document.getElementById("history").lastChild.remove();
    total--;
  }
  document.getElementById("history-header").after(password);
});
